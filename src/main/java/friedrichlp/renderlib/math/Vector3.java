/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.math;

import friedrichlp.renderlib.util.Temporary;

public class Vector3 extends Vector2 {
	private static final Vector3 ZERO = new Vector3(0, 0, 0);
	private static final Vector3 ONE = new Vector3(1, 1, 1);
	private static final Temporary<Vector3> tmp = new Temporary<Vector3>(Vector3.class, 10000);

	public static Vector3 lerp(Vector3 a, Vector3 b, float t) {
		Vector3 diff = b.copy().sub(a);
		return a.copy().add(diff.mul(t));
	}

	public static Vector3 ZERO() {
		return ZERO.copy();
	}

	public static Vector3 ONE() {
		return ONE.copy();
	}

	public static Vector3 TEMPORARY() {
		return tmp.get().setZero();
	}

	public static Vector3 TEMPORARY(float x, float y, float z) {
		return tmp.get().set(x, y, z);
	}

	public static Vector3 TEMPORARY(Vector3 v) {
		return tmp.get().set(v);
	}


	public float z;

	public Vector3() {
	}

	public Vector3(float x, float y, float z) {
		super(x, y);
		this.z = z;
	}

	public Vector3(double x, double y, double z) {
		super(x, y);
		this.z = (float) z;
	}

	public Vector3 add(Vector3 v) {
		return add(v.x, v.y, v.z);
	}

	public Vector3 add(float f) {
		return add(f, f, f);
	}

	public Vector3 add(float x, float y, float z) {
		this.x += x;
		this.y += y;
		this.z += z;
		return this;
	}

	public Vector3 sub(Vector3 v) {
		return sub(v.x, v.y, v.z);
	}

	public Vector3 sub(float f) {
		return sub(f, f, f);
	}

	public Vector3 sub(float x, float y, float z) {
		this.x -= x;
		this.y -= y;
		this.z -= z;
		return this;
	}

	public Vector3 mul(Vector3 v) {
		return mul(v.x, v.y, v.z);
	}

	public Vector3 mul(float f) {
		return mul(f, f, f);
	}

	public Vector3 mul(float x, float y, float z) {
		this.x *= x;
		this.y *= y;
		this.z *= z;
		return this;
	}

	public Vector3 div(Vector3 v) {
		return div(v.x, v.y, v.z);
	}

	public Vector3 div(float f) {
		return div(f, f, f);
	}

	public Vector3 div(float x, float y, float z) {
		this.x /= x;
		this.y /= y;
		this.z /= z;
		return this;
	}

	public Vector3 setZero() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		return this;
	}

	public boolean isZero() {
		return this.x == 0 && this.y == 0 && this.z == 0;
	}

	public Vector3 set(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		return this;
	}

	public Vector3 set(Vector3 v) {
		return set(v.x, v.y, v.z);
	}

	public float distanceTo(Vector3 v) {
		float x = v.x - this.x;
		float y = v.y - this.y;
		float z = v.z - this.z;
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public float magnitude() {
		return (float) Math.sqrt(x * x + y * y + z * z);
	}

	public float dot(Vector3 v) {
		return x * v.x + y * v.y + z * v.z;
	}

	public Vector3 cross(Vector3 v) {
		Vector3 d = new Vector3();
		d.x = y * v.z - z * v.y;
		d.x = v.x * z - v.z * x;
		d.x = x * v.y - y * v.x;
		return d;
	}

	public Vector3 normalize() {
		return div(magnitude());
	}

	public Vector3 copy() {
		return new Vector3(this.x, this.y, this.z);
	}

	@Override
	public String toString() {
		return String.format("Vector3(%s, %s, %s)", x, y, z);
	}
}
