/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model;

import friedrichlp.renderlib.library.TextureType;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MaterialCollection {
	public Object2ObjectArrayMap<String, Material> materials = new Object2ObjectArrayMap<String, Material>();
	public int loadedTextureTypes;

	private MaterialCollection() {}

	public static MaterialCollection loadFromMtls(IFileContainer[] mtls) throws IOException {
		MaterialCollection c = new MaterialCollection();

		for (IFileContainer file : mtls) {
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getStream()))) {
				String line;
				Material m = null;

				while ((line = reader.readLine()) != null) {
					if (line.startsWith("#")) continue;
					if (line.length() == 0) continue;
					String[] args = line.split(" ");
					switch (args[0]) {
						case "newmtl":
							if (m != null) {
								c.materials.put(m.name, m);
							}
							m = new Material();
							m.name = args[1];
							break;
						case "Ka":
							m.Ka = new float[4];
							m.Ka[0] = Float.parseFloat(args[1]);
							m.Ka[1] = Float.parseFloat(args[2]);
							m.Ka[2] = Float.parseFloat(args[3]);
							if (args.length > 4) {
								m.Ka[3] = Float.parseFloat(args[4]);
							} else {
								m.Ka[3] = 1.0f;
							}
							break;
						case "Kd":
							m.Kd = new float[4];
							m.Kd[0] = Float.parseFloat(args[1]);
							m.Kd[1] = Float.parseFloat(args[2]);
							m.Kd[2] = Float.parseFloat(args[3]);
							if (args.length > 4) {
								m.Kd[3] = Float.parseFloat(args[4]);
							} else {
								m.Kd[3] = 1.0f;
							}
							break;
						case "Ks":
							m.Ks = new float[4];
							m.Ks[0] = Float.parseFloat(args[1]);
							m.Ks[1] = Float.parseFloat(args[2]);
							m.Ks[2] = Float.parseFloat(args[3]);
							if (args.length > 4) {
								m.Ks[3] = Float.parseFloat(args[4]);
							} else {
								m.Ks[3] = 1.0f;
							}
							break;
						case "map_Kd":
						case "map_Bump":
						case "refl":
							TextureType t = TextureType.fromIdentifier(args[0]);
							m.textures.put(t, file.getRelative(args[1]));
							c.loadedTextureTypes |= 1 << t.ordinal();
							break;
						case "Ns":
						case "Ke":
						case "Ni":
						case "d":
						case "illum":
							break;
						default:
							ConsoleLogger.debug("MTL: ignored line '%s'", line);
							break;
					}
				}

				if (m != null) {
					c.materials.put(m.name, m);
				}
			}
		}

		return c;
	}
}
