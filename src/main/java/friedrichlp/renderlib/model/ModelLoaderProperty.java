/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.model;

import friedrichlp.renderlib.caching.ICacheData;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;

public class ModelLoaderProperty {
	public float darken;
	protected Object2ObjectArrayMap<String, ICacheData> additionalData = new Object2ObjectArrayMap<>();

	public ModelLoaderProperty(float darken) {
		this.darken = darken;
	}

	public ModelLoaderProperty addAdditionalCacheData(Class<? extends ICacheData> clazz) {
		ICacheData d;
		try {
			d = clazz.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			e.printStackTrace();
			return this;
		}

		additionalData.put(clazz.getName(), d);

		return this;
	}
}
