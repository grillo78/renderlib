/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.caching.ICacheData;
import friedrichlp.renderlib.library.RenderEffect;
import friedrichlp.renderlib.math.HitBox3;
import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.QuadConsumer;
import friedrichlp.renderlib.util.StandardFileContainer;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import it.unimi.dsi.fastutil.objects.ObjectSet;

import java.io.File;

public class ModelInfo {
    protected Model model;

    protected ModelInfo(Model model) {
        this.model = model;
    }

    public void forceLoad(Runnable onLoad) {
        model.onLoad = onLoad;
        model.load();
    }

    public void forceUnload() {
        model.unload();
    }

    public <T extends ICacheData> T getAdditionalCacheData(Class<? extends ICacheData> clazz) {
        return (T)model.data.getAdditionalCacheData(clazz);
    }

    public void addAdditionalHashedFile(File file) {
        addAdditionalHashedFile(new StandardFileContainer(file));
    }

    public void addAdditionalHashedFile(IFileContainer fc) {
        model.data.addAdditionalHashedFile(fc);
    }

    public void addRenderEffect(RenderEffect effect) {
        model.renderEffects |= (1 << effect.ordinal());
    }

    public void removeRenderEffect(RenderEffect effect) {
        model.renderEffects &= ~(1 << effect.ordinal());
    }

    public HitBox3 getHitbox() {
        return model.getHitbox();
    }

    /**
     * Returns a HitBox that perfectly fits all the given parts
     */
    public HitBox3 getHitbox(Iterable<String> parts) {
        HitBox3 box = new HitBox3();
        for (String part : parts) {
            Model.Group g = model.data.groups.get(part);
            if (g != null) {
                box.combine(g.hitbox);
            }
        }
        return box;
    }

    public ObjectSet<String> getPartNames() {
        return model.groups.keySet();
    }

    /**
     * NOTE: modifying the returned map may result in undefined behavior. NEVER EVER modify this
     * map. It is not immutable because of saving otherwise unnecessarily wasted memory and GC time.
     */
    public Object2ObjectArrayMap<String, int[]> getParts() {
        return model.groups;
    }

    /**
     * A mesh hook will be called with (vertices, vertexNormals, vertexUVCoords, faceVerts) once a model is loaded from disk.
     * This can be used to calculate values using the mesh data. It is recommended to cache the result using a custom
     * ICacheData class since this method is only called when there is no active cache for the model.
     */
    public void registerMeshHook(QuadConsumer<FloatArrayList, FloatArrayList, FloatArrayList, IntArrayList> hook) {
        model.meshHooks.add(hook);
    }

    public String getNameHash() {
        return model.data.getNameHashString();
    }
}
