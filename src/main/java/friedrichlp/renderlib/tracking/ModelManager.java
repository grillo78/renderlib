/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.model.ModelLoaderProperty;
import friedrichlp.renderlib.util.ConsoleLogger;
import friedrichlp.renderlib.util.IFileContainer;
import friedrichlp.renderlib.util.StandardFileContainer;
import it.unimi.dsi.fastutil.ints.Int2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.io.File;

public class ModelManager {
	private static Int2ObjectAVLTreeMap<Model> models = new Int2ObjectAVLTreeMap<>();
	private static int currentModelIdx = 0;

	public static ModelInfo registerModel(File modelFile, ModelLoaderProperty modelProperties) {
		return internal_registerModel(new StandardFileContainer(modelFile), modelProperties);
	}

	public static ModelInfo registerModel(IFileContainer fc, ModelLoaderProperty modelProperties) {
		return internal_registerModel(fc, modelProperties);
	}

	private static ModelInfo internal_registerModel(IFileContainer fc, ModelLoaderProperty modelProperties) {
		synchronized (models) {
			String modelName = fc.getPath();

			if (models.values().stream().anyMatch(mdl -> mdl.identifier.equals(fc.getPath()))) {
				ConsoleLogger.error("Tried to register the same model twice: " + modelName);
				return null;
			}

			Model model = new Model(fc, modelProperties, modelName, currentModelIdx++);
			models.put(model.id, model);
			ConsoleLogger.debug("Registered model %s", modelName);
			return new ModelInfo(model);
		}
	}

	public static void unloadAllModels() {
		synchronized (models) {
			models.values().forEach(Model::unload);
		}
	}

	protected static Model getModel(int id) {
		synchronized (models) {
			return models.get(id);
		}
	}
}
