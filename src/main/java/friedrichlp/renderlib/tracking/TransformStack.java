/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.tracking;

import friedrichlp.renderlib.oglw.GLBuffers;
import friedrichlp.renderlib.oglw.GLDrawSetup;
import friedrichlp.renderlib.render.VertexArray;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

public class TransformStack {
	public VertexArray transformCache = new VertexArray(GLBuffers.Target.VERTEX_ARRAY);

	public void updateData(ObjectArrayList<RenderObject> objects) {
		ByteBuffer transformBufB = BufferUtils.createByteBuffer(objects.size() * 16 * 4);

		FloatBuffer transformBuf = transformBufB.asFloatBuffer();
		for (RenderObject obj : objects) {
			if (!obj.isVisible) continue;
			transformBuf.put(obj.modelTransform.f);
		}
		transformBuf.flip();

		transformCache.set(transformBufB);
	}

	public void bind() {
		transformCache.bind();
		GLDrawSetup.vertexAttribPointer(0, 4, GLDrawSetup.Type.FLOAT, false, 4 * 16, 0);
		GLDrawSetup.vertexAttribPointer(1, 4, GLDrawSetup.Type.FLOAT, false, 4 * 16, 4 * 4);
		GLDrawSetup.vertexAttribPointer(2, 4, GLDrawSetup.Type.FLOAT, false, 4 * 16, 4 * 8);
		GLDrawSetup.vertexAttribPointer(3, 4, GLDrawSetup.Type.FLOAT, false, 4 * 16, 4 * 12);
		GLDrawSetup.vertexAttribDivisor(0, 1);
		GLDrawSetup.vertexAttribDivisor(1, 1);
		GLDrawSetup.vertexAttribDivisor(2, 1);
		GLDrawSetup.vertexAttribDivisor(3, 1);
	}
}
