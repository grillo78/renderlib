/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import java.io.*;

public class FileContainer {
	public final File file;
	public final boolean isResource;

	public FileContainer(File file, boolean isResource) {
		this.file = file;
		this.isResource = isResource;
	}

	public InputStream getStream() {
		try {
			if (isResource) return getClass().getResourceAsStream(file.getPath().replace("\\", "/"));
			return new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public BufferedReader getReader() {
		try {
			if (isResource) return new BufferedReader(new InputStreamReader(getStream()));
			return new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public FileContainer getRelative(String relPath) {
		FileContainer fc = new FileContainer(new File(file.getParent(), relPath), isResource);
		return fc;
	}
}
