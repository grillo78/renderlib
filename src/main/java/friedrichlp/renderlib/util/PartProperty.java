/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import friedrichlp.renderlib.math.Matrix4f;
import friedrichlp.renderlib.math.Vector3;

public class PartProperty {
	public boolean changed;

	private Vector3 position = Vector3.ZERO();
	private Vector3 rotation = Vector3.ZERO();
	private Vector3 scale = Vector3.ONE();
	private Vector3 origin = Vector3.ZERO();
	private Matrix4f transform = new Matrix4f();
	private boolean hidden = false;

	public Matrix4f getTransformMatrix() {
		return transform;
	}

	public Vector3 getPosition() {
		return position;
	}

	public Vector3 getRotation() {
		return rotation;
	}

	public Vector3 getScale() {
		return scale;
	}

	public Vector3 getOrigin() {
		return origin;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setOrigin(float x, float y, float z) {
		origin.set(x, y, z);
	}

	public void translate(float x, float y, float z) {
		synchronized (position) {
			position.add(x, y, z);
		}
		changed = true;
	}

	public void rotate(float x, float y, float z) {
		synchronized (rotation) {
			rotation.add(x, y, z);
		}
		changed = true;
	}

	/*public void rotate(float x, float y, float z, Vector3 origin){
		rotation.add(x, y, z);
		updateTransform();
	}*/

	public void scale(float x, float y, float z) {
		synchronized (scale) {
			scale.mul(x, y, z);
		}
		changed = true;
	}

	public void setPosition(float x, float y, float z) {
		synchronized (position) {
			position.set(x, y, z);
		}
		changed = true;
	}

	public void setRotation(float x, float y, float z) {
		synchronized (rotation) {
			rotation.set(x, y, z);
		}
		changed = true;
	}

	public void setScale(float x, float y, float z) {
		synchronized (scale) {
			scale.set(x, y, z);
		}
		changed = true;
	}

	public synchronized void setTransform(Vector3 position, Vector3 rotation, Vector3 scale) {
		if (ValidationUtil.isNull(position, () -> ConsoleLogger.warn("position input was null!"))) return;
		if (ValidationUtil.isNull(rotation, () -> ConsoleLogger.warn("rotation input was null!"))) return;
		if (ValidationUtil.isNull(scale, () -> ConsoleLogger.warn("scale input was null!"))) return;
		this.position.set(position);
		this.rotation.set(rotation);
		this.scale.set(scale);
		changed = true;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public void resetTransform() {
		position.setZero();
		rotation.setZero();
		scale.setZero();
		transform.setIdentity();
	}

	public void applyTransformUpdates() {
		if (!changed) return;
		changed = false;

		transform.setIdentity();
		transform.multiply(Matrix4f.SCALE(scale.x, scale.y, scale.z));
		if (!origin.isZero()) transform.multiply(Matrix4f.TRANSLATE(origin.x, origin.y, origin.z));
		if (rotation.x != 0) transform.multiply(Matrix4f.TEMPORARY().rotate(rotation.x, 1, 0, 0));
		if (rotation.y != 0) transform.multiply(Matrix4f.TEMPORARY().rotate(rotation.y, 0, 1, 0));
		if (rotation.z != 0) transform.multiply(Matrix4f.TEMPORARY().rotate(rotation.z, 0, 0, 1));
		if (!origin.isZero()) transform.multiply(Matrix4f.TRANSLATE(-origin.x, -origin.y, -origin.z));
		transform.multiply(Matrix4f.TRANSLATE(position.x, position.y, position.z));
	}
}
