/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import java.io.*;

public class StandardFileContainer implements IFileContainer {
	private File file;

	public StandardFileContainer(File file) {
		this.file = file;
	}

	@Override
	public InputStream getStream() throws IOException {
		return new FileInputStream(file);
	}

	public StandardFileContainer getRelative(String path) {
		return new StandardFileContainer(new File(file.getParent(), path));
	}

	@Override
	public String getPath() {
		return file.getPath();
	}

	@Override
	public String getName() {
		return file.getName();
	}

	@Override
	public void save(DataOutputStream out) throws IOException {
		out.writeUTF(file.getPath());
	}

	@Override
	public void load(DataInputStream in) throws IOException {
		file = new File(in.readUTF());
	}
}
