/*
 * RenderLib
 * Copyright (C) 2019 - 2020 Friedrich
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

package friedrichlp.renderlib.util;

import sun.misc.Unsafe;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class UnsafeUtil {
	private static Unsafe unsafe;

	static {
		try {
			Field f = Unsafe.class.getDeclaredField("theUnsafe");
			f.setAccessible(true);
			unsafe = (Unsafe) f.get(null);
		} catch (Exception e) {
		}
	}

	public static Object createInstance(Class<?> clazz, boolean printException, Object... parameters) {
		try {
			if (parameters.length == 0) {
				return unsafe.allocateInstance(clazz);
			}

			Class<?>[] parameterTypes = new Class<?>[parameters.length];
			for (int i = 0; i < parameters.length; i++) {
				parameterTypes[i] = parameters[i].getClass();
			}
			Constructor<?> ctor = clazz.getDeclaredConstructor(parameterTypes);
			return ctor.newInstance(parameters);
		} catch (Exception e) {
			if (printException) e.printStackTrace();
		}
		return null;
	}

	public static void memcpy(long src, long dst, long count) {
		unsafe.copyMemory(src, dst, count);
	}
}
