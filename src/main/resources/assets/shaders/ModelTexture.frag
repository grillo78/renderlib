#version 150

##defines##

#define GAMMA 2.2
#define GAMMA_VEC vec3(1.0 / GAMMA)

//settings for render effects
//ambient occlusion
#define AO_STRENGTH 1
#define AO_SAMPLES 20
#define AO_RADIUS 0.5
#define AO_CLAMP 0.125
#define AO_NOISE 0.0002
#define AO_DIFFAREA 0.3
#define AO_GDISPLCE 0.4

const float PI = 3.14159265359;

uniform sampler2D texture;
uniform sampler2D bumpMap;
uniform sampler2D metallicMap;
uniform sampler2D roughnessMap;

in vec2 texCoord;
in vec4 uvSize;
flat in int colorOverride;
in vec3 normal;
in vec3 fragPos;
in vec3 camDirTS;
//in mat3 tangentBasis;

out vec4 gl_FragColor;

uniform vec3 camPos;

// graphic settings
uniform float ambientLightStrength;
uniform float bumpStrength;

bool noise = true;
float width = 0;
float height = 0;
float zFar = 100;
float zNear = 0;

vec2 correctedUV;

float distributionGGX(vec3 N, vec3 H, float roughness) {
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float geometrySchlickGGX(float NdotV, float roughness) {
    float r = (roughness + 1.0);
    float k = (r * r) / 8.0;

    float num = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}
float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness) {
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2 = geometrySchlickGGX(NdotV, roughness);
    float ggx1 = geometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 getNormalFromMap() {
    vec3 tangentNormal = texture2D(bumpMap, correctedUV).xyz * 2.0 - 1.0;

    vec3 Q1 = dFdx(fragPos);
    vec3 Q2 = dFdy(fragPos);
    vec2 st1 = dFdx(correctedUV);
    vec2 st2 = dFdy(correctedUV);

    vec3 N = normalize(normal);
    vec3 T = normalize(Q1*st2.t - Q2*st1.t);
    vec3 B = -normalize(cross(N, T));
    mat3 TBN = mat3(T, B, N);

    return normalize(TBN * tangentNormal);
}

// NOTE: the current pbr code is a mess and does not fully work. I will update it in the future
void main(){
    if(colorOverride == 1) { //if no texture is there override the color. uvSize will now contain the color information
        gl_FragColor = uvSize;
    } else {
        correctedUV = fract(texCoord.xy) * uvSize.zw + uvSize.xy;
        vec4 baseColor = texture2D(texture, correctedUV);
        vec3 albedo = baseColor.rgb;
        float metallic = 0.0;
        float roughness = 0.5;
        float ao = 1.0;
        vec3 color;

        #ifdef AMBIENT_OCCLUSION
            //ao = getAO(correctedUV);
        #endif

        #ifdef NORMAL_LIGHTING
            #ifdef NORMAL_MAP
                vec3 N = normalize(texture2D(bumpMap, correctedUV).rgb * 2.0 - 1.0) * bumpStrength;
                vec3 V = camDirTS;
            #else
                vec3 N = normal;
                vec3 V = normalize(camPos - fragPos);
            #endif

            #ifdef PBR // do full pbr calculations
                #ifdef METALLIC_MAP
                    metallic = texture2D(metallicMap, correctedUV).r;
                #endif

                N = getNormalFromMap();
                V = normalize(camPos - fragPos);

                vec3 F0 = mix(vec3(0.04), albedo, metallic);

                vec3 L = V; // this works because the camera position currently is equal to the light position
                vec3 H = normalize(V + L);
                float distance = length(camPos - fragPos) * 0.1;
                float attenuation = 1.0 / (distance * distance);
                vec3 radiance = vec3(attenuation);

                // cook-torrance brdf
                float NDF = distributionGGX(N, H, roughness);
                float G = geometrySmith(N, V, L, roughness);
                vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);

                vec3 kS = F;
                vec3 kD = vec3(1.0) - kS;
                kD *= 1.0 - metallic;

                vec3 numerator = NDF * G * F;
                float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
                vec3 specular = numerator / max(denominator, 0.001);

                float NdotL = max(dot(N, L), 0.0);
                vec3 Lo = (kD * albedo / PI + specular) * radiance * NdotL;

                vec3 ambient = 0.03 * albedo * ao;
                color = ambient + Lo;
            #else // calculate default normal lighting
                float normalLight = max(dot(N, V), 0.0) * 2.0; // HINT: changed clamp to max
                color = albedo * (vec3(normalLight) + ambientLightStrength);
            #endif
        #else
            color = albedo;
        #endif

        // gamma correction
        color = pow(color, GAMMA_VEC);
        gl_FragColor = vec4(color.rgb, baseColor.a);
    }
}
