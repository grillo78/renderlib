#version 150
#extension GL_ARB_explicit_attrib_location : enable
#define EPSILON 0.00001

##defines##

layout(location = 0) in mat4 modelTransform;

layout(location = 4) in float mtlIndex;
layout(location = 5) in vec3 modelVert;
layout(location = 6) in vec3 modelNorm;
layout(location = 7) in vec2 modelTexCoord;

layout(location = 8) in vec3 tangent;
layout(location = 9) in vec3 biTangent;

out vec2 texCoord;
out vec4 uvSize;
flat out int colorOverride;
out vec3 normal;
out vec3 fragPos; // fragment positon in world space
out vec3 camDirTS;
//out mat3 tangentBasis;

uniform mat4 modelViewProjection;
uniform mat4 modelView;
uniform mat4 uniformModelTransform;
uniform vec3 camPos;

layout(std140) uniform MtlUvBlock {
	vec4 mtlUvSizes[512];
};

void main() {
	#ifdef INSTANCED_DRAW
		mat4 m = modelTransform;
	#else
		mat4 m = uniformModelTransform;
	#endif

	gl_Position = modelViewProjection * m * vec4(modelVert, 1.0);
	fragPos = (m * vec4(modelVert, 1.0)).xyz;

	texCoord = modelTexCoord;
	normal = modelNorm;

	int mtlIdx = int(mtlIndex) & 0xFFFF;
	colorOverride = mtlIdx >> 15;
	mtlIdx -= 32768 * colorOverride;
	uvSize = mtlUvSizes[mtlIdx];

	#ifdef NORMAL_MAP
		vec3 camDirCS = vec3(0.0) - (modelView * m * vec4(modelVert, 1.0)).xyz;

		mat3 modelView3x3 = mat3(modelView);
		vec3 tangentCS = modelView3x3 * tangent;
		vec3 biTangentCS = modelView3x3 * biTangent;
		vec3 normalCS = modelView3x3 * modelNorm;

		mat3 TBN = transpose(mat3(tangentCS, biTangentCS, normalCS));

		camDirTS = TBN * camDirCS;
	#endif

	#ifdef PBR
		//tangentBasis = mat3(m) * mat3(tangent, biTangent, modelNorm);
	#endif
}
